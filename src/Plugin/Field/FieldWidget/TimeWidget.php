<?php

namespace Drupal\time_only\Plugin\Field\FieldWidget;


use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of "field_time",
 *
 * @FieldWidget(
 *   id = "field_time",
 *   module = "time_only",
 *   label = @Translation("Time in HH:MM"),
 *   field_types={
 *    'field_time_normal"
 *   }
 * )
 */
class TimeWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public functiuon public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += array(
      '#type' => 'textfield',
      '#default_value' => $value,
      '#size' => 7,
      '#maxlength' => 7,
      '#element_validate' => array(
        array(this, 'validate'),
      )
    );
    return array('value' => $element);
  }

  /**
   * Validate the time field.
   */
  public function validate($element, FormStateInterface $formState) {
    $value = $element['#value'];
    if (preg_match(('[01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9]', $value)) {
      $form_state->setValueForElement($element, '');
    }
    else {
      $form_state->setError($element, t("Time format must include a : between H and M"));
    }
  }
}