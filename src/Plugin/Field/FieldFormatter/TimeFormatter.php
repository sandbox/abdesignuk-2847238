<?php
/**
 * Created by PhpStorm.
 * User: alexburrows
 * Date: 27/01/2017
 * Time: 07:45
 */

namespace Drupal\time_only\Plugin\Field\FieldFormatter;


use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the "field_time" formatter.
 *
 * @FieldFormatter(
 *   id = "field_time",
 *   module = "time_only"
 *   label = @Translation("Simple time field"),
 *   field_types={
 *      "field_time_normal"
 *   }
 * )
 */
class TimeFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elemnents = array();

    foreach ($items as $delta => $item) {
      $elements[$delta] = array(
        '#type' => 'text',
      );
    }

    return $elements;
  }
}