<?php
/**
 * Created by PhpStorm.
 * User: alexburrows
 * Date: 27/01/2017
 * Time: 07:50
 */

namespace Drupal\time_only\Controller\TimeFieldController;


use Drupal\Core\Controller\ControllerBase;

/**
 * Return repsonses for dblog routes.
 */
class FieldTimeController extends ControllerBase {

  /**
   * A simple page to explain to the developer what to do.
   */
  public function description() {
    return array(
      '#markup' => t(
        "The time field provides a field component to allow for just time"
      ),
    );
  }
}